package sk.golias.webfluxstockquoteservice.service;

import reactor.core.publisher.Flux;
import sk.golias.webfluxstockquoteservice.model.Quote;

import java.time.Duration;

public interface QuoteGeneratorService {

    Flux<Quote> fetchQuoteStream(Duration period);
}
